import shapes2d.Circle;
import shapes2d.Square;
import shapes3d.Cube;
import shapes3d.Cylinder;

import java.util.Scanner;

// IntelliJ Idea üzerinde bu class'ı çalıştıramıyorum ancak git üzerinden çalıştırabiliyorum.

public class TestHomework {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);

        System.out.print("Enter circle radius:");
        double radius = myObj.nextDouble();
        Circle circ = new Circle(radius);
        System.out.println(circ);


        System.out.print("Enter square size:");
        double size = myObj.nextDouble();
        Square squ = new Square(size);
        System.out.println(squ);


        System.out.print("Enter cylinder radius:");
        double cyradius = myObj.nextDouble();
        System.out.print("Enter cylinder length:");
        double length = myObj.nextDouble();
        Cylinder cyl = new Cylinder(cyradius, length);
        System.out.println(cyl);

        System.out.print("Enter cube size:");
        double c1size = myObj.nextDouble();
        Cube c1 = new Cube(c1size);
        System.out.println(c1);
    }
}
