package shapes2d;

import java.util.Scanner;

public class Circle
{
    double radius;

    public Circle(double r)
    {
        radius = r;
    }

    public void setRadius(double r)
    {
        this.radius = r;
    }

    public double getRadius()
    {
        return radius;
    }
    public double getArea()
    {
        return Math.PI * radius * radius;
    }
    public double getCircumference()
    {
        return 2 * Math.PI * radius;
    }
    public double getDiameter()
    {
        return radius * 2;
    }

    @Override
    public String toString()
    {
        return "R= " + this.getRadius()
                + "\nArea= " + this.getArea()
                + "\nCircumference= " + this.getCircumference()
                + "\nDiameter= " + this.getDiameter();
    }
}
