package shapes2d;

import java.util.Scanner;

public class Square
{
    double size;

    public Square(double s)
    {
        size = s;
    }

    public void setSize(double s)
    {
        this.size = s;
    }

    public double getSize()
    {
        return size;
    }
    public double getArea()
    {
        return size * size;
    }
    public double getCircumference()
    {
        return 4 * size;
    }

    @Override
    public String toString()
    {
        return "Size= " + this.getSize()
                + "\nArea= " + this.getArea()
                + "\nCircumference= " + this.getCircumference();
    }
}
