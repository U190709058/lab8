package shapes3d;

import shapes2d.Square;

public class Cube extends Square {

    public Cube(double s) {
        super(s);
    }

    public double getSize()
    {
        return super.getSize();
    }
    public double getArea()
    {
        return 6 * super.getSize();
    }
    public double getVolume()
    {
        return super.getArea() * super.getSize();
    }

    @Override
    public String toString()
    {
        return "Size= " + this.getSize()
                + "\nArea= " + this.getArea()
                + "\nVolume= " + this.getVolume();
    }
}