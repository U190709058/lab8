package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    public double length;

    public Cylinder(double r, double length) {
        super(r);
        this.length = length;
    }

    public double getLength()
    {
        return length;
    }
    public double getArea()
    {
        return super.getCircumference() * (super.getRadius() + length);
    }
    public double getVolume()
    {
        return length * super.getArea();
    }

    @Override
    public String toString()
    {
        return "Length= " + this.getLength()
                + "\nR= " + super.getRadius()
                + "\nArea= " + this.getArea()
                + "\nVolume= " + this.getVolume();
    }
}